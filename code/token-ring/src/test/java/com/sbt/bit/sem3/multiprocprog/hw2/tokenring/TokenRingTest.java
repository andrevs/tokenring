package com.sbt.bit.sem3.multiprocprog.hw2.tokenring;

import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.consumers.PrintConsumer;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packet.NetworkPacket;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packet.NetworkPacketImpl;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Тест {@link TokenRing}
 *
 * @author Vasyukevich Andrey
 * @since 26.11.2017
 */
public class TokenRingTest {
    @Test
    public void test() throws InterruptedException {
        NetworkPacket packet1 = new NetworkPacketImpl(3, "data1");
        NetworkPacket packet2 = new NetworkPacketImpl(3, "data2");

        final List<NetworkPacket> receivedOnNode3 = Collections.synchronizedList(new ArrayList<NetworkPacket>());

        TokenRing tokenRing = new TokenRingImpl(5);

        tokenRing.attachConsumerToNode(3, receivedOnNode3::add);

        for (int i = 0; i < tokenRing.size(); ++i) {
            tokenRing.attachConsumerToNode(i, new PrintConsumer());
        }

        tokenRing.getNodeAsConsumer(0).consume(packet1);
        tokenRing.getNodeAsConsumer(4).consume(packet2);

        while (receivedOnNode3.size() < 2) {
            Thread.sleep(100);
        }

        assertEquals(2, receivedOnNode3.size());
        assertEquals(packet1.getData(), receivedOnNode3.get(0).getData());
        assertEquals(packet2.getData(), receivedOnNode3.get(1).getData());
    }
}
