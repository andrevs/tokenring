package com.sbt.bit.sem3.multiprocprog.hw2.tokenring.node;

import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.consumers.Consumer;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.consumers.ConsumerObservable;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.TokenRing;

/**
 * Node interface of {@link TokenRing}
 *
 * @author Vasyukevich Andrey
 * @since 25.11.2017
 */
public interface Node extends ConsumerObservable, Consumer {
    int getIndex();
}
