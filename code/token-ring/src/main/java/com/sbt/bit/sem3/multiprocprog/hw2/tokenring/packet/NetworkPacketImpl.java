package com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packet;

/**
 * @author Vasyukevich Andrey
 * @since 25.11.2017
 */
public class NetworkPacketImpl extends PacketImpl<String> implements NetworkPacket {
    public NetworkPacketImpl(int destinationIndex, String data) {
        super(destinationIndex, data);
    }
}
