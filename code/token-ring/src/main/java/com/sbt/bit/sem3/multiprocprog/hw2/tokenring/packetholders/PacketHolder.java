package com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packetholders;

import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packet.NetworkPacket;

import java.util.concurrent.TimeUnit;

/**
 * Packet holder transfers packets from one node to another.
 *
 * @author Vasyukevich Andrey
 * @since 25.11.2017
 */
public interface PacketHolder {
    void add(NetworkPacket packet);
    NetworkPacket poll(long timeout, TimeUnit timeUnit) throws InterruptedException;
}
