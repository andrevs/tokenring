package com.sbt.bit.sem3.multiprocprog.hw2.tokenring.node;

import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packet.NetworkPacket;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packetholders.PacketHolder;

import java.util.concurrent.TimeUnit;

/**
 * @author Vasyukevich Andrey
 * @since 26.11.2017
 */
public class NodeTaskImpl implements NodeTask {
    private final PacketHolder packetHolderFromPreviousNode;
    private final Node node;
    private final long timeout;
    private final TimeUnit timeUnit;

    public NodeTaskImpl(PacketHolder packetHolderFromPreviousNode, Node node, long timeout, TimeUnit timeUnit) {
        this.packetHolderFromPreviousNode = packetHolderFromPreviousNode;
        this.node = node;
        this.timeout = timeout;
        this.timeUnit = timeUnit;
    }

    @Override
    public void run() {
        try {
            while (true) {
                NetworkPacket packet = packetHolderFromPreviousNode.poll(timeout, timeUnit);
                if (packet != null) {
                    node.consume(packet);
                }
            }
        } catch (InterruptedException e) {
            System.err.format("Node[index=%2d] is interrupted\n", node.getIndex());
        }
    }
}
