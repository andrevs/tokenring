package com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packet;

/**
 * @author Vasyukevich Andrey
 * @since 25.11.2017
 */
public class PacketImpl<DataType> implements Packet<DataType> {
    private final int destinationIndex;
    private final DataType data;

    public PacketImpl(int destinationIndex, DataType data) {
        this.destinationIndex = destinationIndex;
        this.data = data;
    }

    @Override
    public int getDestinationIndex() {
        return destinationIndex;
    }

    @Override
    public DataType getData() {
        return data;
    }
}
