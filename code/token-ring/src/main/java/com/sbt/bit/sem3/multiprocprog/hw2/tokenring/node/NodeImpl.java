package com.sbt.bit.sem3.multiprocprog.hw2.tokenring.node;

import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.consumers.ConsumerObservableOnArrayList;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packet.NetworkPacket;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packetholders.PacketHolder;

/**
 * @author Vasyukevich Andrey
 * @since 25.11.2017
 */
public class NodeImpl extends ConsumerObservableOnArrayList implements Node {
    private final int index;
    private final PacketHolder packetHolderToNextNode;

    public NodeImpl(int index, PacketHolder packetHolderToNextNode) {
        this.index = index;
        this.packetHolderToNextNode = packetHolderToNextNode;
    }

    @Override
    public int getIndex() {
        return index;
    }

    @Override
    public void consume(NetworkPacket packet) {
        if (packet.getDestinationIndex() == index) {
            triggerConsumers(packet);
        } else {
            packetHolderToNextNode.add(packet);
        }
    }
}
