package com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packetholders;

import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packet.NetworkPacket;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author Vasyukevich Andrey
 * @since 25.11.2017
 */
public class PacketHolderOnBlockingQueue implements PacketHolder {
    private final BlockingQueue<NetworkPacket> blockingQueue;

    public PacketHolderOnBlockingQueue(BlockingQueue<NetworkPacket> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void add(NetworkPacket packet) {
        blockingQueue.add(packet);
    }

    @Override
    public NetworkPacket poll(long timeout, TimeUnit timeUnit) throws InterruptedException {
        return blockingQueue.poll(timeout, timeUnit);
    }
}
