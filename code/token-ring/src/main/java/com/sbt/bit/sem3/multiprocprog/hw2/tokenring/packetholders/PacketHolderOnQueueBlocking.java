package com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packetholders;

import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packet.NetworkPacket;

import java.util.Queue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Vasyukevich Andrey
 * @since 25.11.2017
 */
public class PacketHolderOnQueueBlocking implements PacketHolder {
    private final Queue<NetworkPacket> queue;

    private final Lock lock = new ReentrantLock();
    private final Condition notEmpty = lock.newCondition();

    public PacketHolderOnQueueBlocking(Queue<NetworkPacket> queue) {
        this.queue = queue;
    }

    @Override
    public void add(NetworkPacket packet) {
        lock.lock();
        try {
            queue.add(packet);
            notEmpty.signal();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public NetworkPacket poll(long timeout, TimeUnit timeUnit) throws InterruptedException {
        long nanos = timeUnit.toNanos(timeout);
        lock.lock();
        try {
            while (queue.isEmpty()) {
                if (nanos <= 0) {
                    return null;
                }
                nanos = notEmpty.awaitNanos(nanos);
            }
            return queue.poll();
        } finally {
            lock.unlock();
        }
    }
}
