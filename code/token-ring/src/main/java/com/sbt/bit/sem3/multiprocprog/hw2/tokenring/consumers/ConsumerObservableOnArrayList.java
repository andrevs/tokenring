package com.sbt.bit.sem3.multiprocprog.hw2.tokenring.consumers;

import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packet.NetworkPacket;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author Vasyukevich Andrey
 * @since 25.11.2017
 */
public class ConsumerObservableOnArrayList implements ConsumerObservable {
    private final List<Consumer> consumers = new ArrayList<>();
    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    @Override
    public void attachConsumer(Consumer consumer) {
        readWriteLock.writeLock().lock();
        try {
            consumers.add(consumer);
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }

    @Override
    public void detachConsumer(Consumer consumer) {
        readWriteLock.writeLock().lock();
        try {
            consumers.remove(consumer);
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }

    @Override
    public void triggerConsumers(NetworkPacket packet) {
        readWriteLock.readLock().lock();
        try {
            for (Consumer consumer : consumers) {
                consumer.consume(packet);
            }
        } finally {
            readWriteLock.readLock().unlock();
        }
    }
}
