package com.sbt.bit.sem3.multiprocprog.hw2.tokenring.consumers;

import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packet.NetworkPacket;

/**
 * @author Vasyukevich Andrey
 * @since 25.11.2017
 */
public interface ConsumerObservable {
    void attachConsumer(Consumer consumer);
    void detachConsumer(Consumer consumer);
    void triggerConsumers(NetworkPacket packet);
}
