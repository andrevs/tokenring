package com.sbt.bit.sem3.multiprocprog.hw2.tokenring.node;

/**
 * Node task interface to run each node in its own thread.
 *
 * @author Vasyukevich Andrey
 * @since 26.11.2017
 */
public interface NodeTask extends Runnable {
}
