package com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packet;

/**
 * Packet interface.
 * Packet destination index determines index of target node, on which this packet will be send to consumers.
 *
 * @author Vasyukevich Andrey
 * @since 25.11.2017
 */
public interface Packet<DataType> {
    int getDestinationIndex();
    DataType getData();
}
