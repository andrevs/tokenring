package com.sbt.bit.sem3.multiprocprog.hw2.tokenring.consumers;

import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packet.NetworkPacket;

/**
 * Consumer interface to interact with token ring.
 * One can attach packet consumer to token ring node, and node itself is a packet consumer.
 *
 * @author Vasyukevich Andrey
 * @since 25.11.2017
 */
public interface Consumer {
    void consume(NetworkPacket packet);
}
