package com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packet;

/**
 * Simple NetworkPacket interface which is suitable for our purposes
 *
 * @author Vasyukevich Andrey
 * @since 25.11.2017
 */
public interface NetworkPacket extends Packet<String> {
}
