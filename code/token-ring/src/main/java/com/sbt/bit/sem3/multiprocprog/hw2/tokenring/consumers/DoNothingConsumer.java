package com.sbt.bit.sem3.multiprocprog.hw2.tokenring.consumers;

import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packet.NetworkPacket;

/**
 * @author Vasyukevich Andrey
 * @since 25.11.2017
 */
public class DoNothingConsumer implements Consumer {
    @Override
    public void consume(NetworkPacket packet) {
        // do nothing
    }
}
