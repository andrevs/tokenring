package com.sbt.bit.sem3.multiprocprog.hw2.tokenring;

import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.consumers.Consumer;

/**
 * Token ring interface
 *
 * @author Vasyukevich Andrey
 * @since 25.11.2017
 */
public interface TokenRing {
    int size();
    void attachConsumerToNode(int nodeIndex, Consumer consumer);
    void detachConsumerFromNode(int nodeIndex, Consumer consumer);
    Consumer getNodeAsConsumer(int nodeIndex);
    void stop();
}
