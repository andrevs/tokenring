package com.sbt.bit.sem3.multiprocprog.hw2.tokenring;

import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.consumers.Consumer;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.node.Node;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.node.NodeImpl;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.node.NodeTask;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.node.NodeTaskImpl;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packetholders.PacketHolder;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packetholders.PacketHolderOnQueueBlocking;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Token ring implementation
 *
 * @author Vasyukevich Andrey
 * @since 25.11.2017
 */
public class TokenRingImpl implements TokenRing {
    private final int size;
    private final List<PacketHolder> packetHolders;
    private final List<Node> nodes = new ArrayList<>();
    private final List<Thread> nodeThreads = new ArrayList<>();

    private long timeout = Integer.MAX_VALUE;
    private TimeUnit timeUnit = TimeUnit.SECONDS;

    public TokenRingImpl(int size) {
        this.size = size;
        this.packetHolders = new ArrayList<>();
        for (int i = 0; i < size; ++i) {
            PacketHolderOnQueueBlocking packetHolder = new PacketHolderOnQueueBlocking(new ArrayDeque<>());
            packetHolders.add(packetHolder);
        }
        init();
    }

    public TokenRingImpl(int size, List<PacketHolder> packetHolders) {
        this.size = size;
        this.packetHolders = packetHolders;
        init();
    }

    public TokenRingImpl(int size, List<PacketHolder> packetHolders, long timeout, TimeUnit timeUnit) {
        this.size = size;
        this.packetHolders = packetHolders;
        this.timeout = timeout;
        this.timeUnit = timeUnit;
        init();
    }

    private void init() {
        for (int nodeIndex = 0; nodeIndex < size; ++nodeIndex) {
            Node node = new NodeImpl(nodeIndex, packetHolders.get(nodeIndex));
            nodes.add(node);
        }

        for (int nodeIndex = 0; nodeIndex < size; ++nodeIndex) {
            int previousNodeIndex = (nodeIndex - 1 + size) % size;
            PacketHolder packetHolderFromPreviousNode = packetHolders.get(previousNodeIndex);
            Node node = nodes.get(nodeIndex);
            NodeTask nodeTask = new NodeTaskImpl(packetHolderFromPreviousNode, node, timeout, timeUnit);
            Thread nodeThread = new Thread(nodeTask);
            nodeThreads.add(nodeThread);
        }

        for (Thread nodeThread : nodeThreads) {
            nodeThread.start();
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void attachConsumerToNode(int nodeIndex, Consumer consumer) {
        nodes.get(nodeIndex).attachConsumer(consumer);
    }

    @Override
    public void detachConsumerFromNode(int nodeIndex, Consumer consumer) {
        nodes.get(nodeIndex).detachConsumer(consumer);
    }

    @Override
    public Consumer getNodeAsConsumer(int nodeIndex) {
        return nodes.get(nodeIndex);
    }

    @Override
    public void stop() {
        for (Thread nodeThread : nodeThreads) {
            nodeThread.interrupt();
        }

        for (Thread nodeThread : nodeThreads) {
            try {
                nodeThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
