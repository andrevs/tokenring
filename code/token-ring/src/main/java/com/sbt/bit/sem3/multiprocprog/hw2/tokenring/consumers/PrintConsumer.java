package com.sbt.bit.sem3.multiprocprog.hw2.tokenring.consumers;

import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packet.NetworkPacket;

import java.io.PrintStream;

/**
 * @author Vasyukevich Andrey
 * @since 25.11.2017
 */
public class PrintConsumer implements Consumer {
    private final PrintStream printStream;

    public PrintConsumer() {
        this.printStream = System.out;
    }

    public PrintConsumer(PrintStream printStream) {
        this.printStream = printStream;
    }

    @Override
    public void consume(NetworkPacket packet) {
        int destinationIndex = packet.getDestinationIndex();
        String data = packet.getData();
        printStream.format("Received NetworkPacket(destinationIndex=%d, data=\"%s\")\n", destinationIndex, data);
    }
}
