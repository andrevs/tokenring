package com.sbt.bit.sem3.multiprocprog.hw2.testing.testsupport;

import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.consumers.Consumer;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packet.NetworkPacketImpl;

import java.util.UUID;

/**
 * @author Vasyukevich Andrey
 * @since 26.11.2017
 */
public class PacketProducerTask implements Runnable {
    private final Consumer consumer;
    private int destinationNodeIndex;
    private int numberOfPackets;

    public PacketProducerTask(Consumer consumer, int destinationNodeIndex, int numberOfPackets) {
        this.consumer = consumer;
        this.destinationNodeIndex = destinationNodeIndex;
        this.numberOfPackets = numberOfPackets;
    }

    @Override
    public void run() {
        for (int i = 0; i < numberOfPackets; ++i) {
            NetworkPacketImpl packet = new NetworkPacketImpl(destinationNodeIndex, UUID.randomUUID().toString());
            consumer.consume(packet);
        }
    }
}
