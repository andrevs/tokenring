package com.sbt.bit.sem3.multiprocprog.hw2.testing.testsupport;

import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.TokenRing;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.consumers.Consumer;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packetholders.PacketHolder;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packetholders.PacketHolderOnBlockingQueue;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packetholders.PacketHolderOnQueueBlocking;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;

/**
 * @author Vasyukevich Andrey
 * @since 26.11.2017
 */
public class Utils {
    public static <TQueue extends Queue> List<PacketHolder> createListOfPacketHoldersOnQueueBlocking(
            int size, Class<TQueue> queueClass) {
        try {
            List<PacketHolder> packetHolders = new ArrayList<>(size);
            for (int i = 0; i < size; ++i) {
                TQueue queue = queueClass.newInstance();
                PacketHolder packetHolder = new PacketHolderOnQueueBlocking(queue);
                packetHolders.add(packetHolder);
            }
            return packetHolders;
        } catch (InstantiationException | IllegalAccessException e) {
            return null;
        }
    }

    public static <TBlockingQueue extends BlockingQueue> List<PacketHolder> createListOfPacketHoldersOnBlockingQueue(
            int size, Class<TBlockingQueue> blockingQueueClass) {
        try {
            List<PacketHolder> packetHolders = new ArrayList<>(size);
            for (int i = 0; i < size; ++i) {
                TBlockingQueue blockingQueue = blockingQueueClass.newInstance();
                PacketHolder packetHolder = new PacketHolderOnBlockingQueue(blockingQueue);
                packetHolders.add(packetHolder);
            }
            return packetHolders;
        } catch (InstantiationException | IllegalAccessException e) {
            return null;
        }
    }

    public static List<Thread> createListOfPacketProducersThreads(TokenRing tokenRing, int producedPacketsPerNode) {
        List<Thread> producerThreads = new ArrayList<>(tokenRing.size());
        for (int i = 0; i < tokenRing.size(); ++i) {
            Consumer consumer = tokenRing.getNodeAsConsumer(i);
            PacketProducerTask producer = new PacketProducerTask(consumer, -1, producedPacketsPerNode);
            Thread producerThread = new Thread(producer);
            producerThreads.add(producerThread);
        }
        return producerThreads;
    }
}
