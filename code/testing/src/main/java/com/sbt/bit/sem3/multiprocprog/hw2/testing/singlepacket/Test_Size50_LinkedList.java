package com.sbt.bit.sem3.multiprocprog.hw2.testing.singlepacket;

import com.sbt.bit.sem3.multiprocprog.hw2.testing.testsupport.PacketConsumeNotifier;
import com.sbt.bit.sem3.multiprocprog.hw2.testing.testsupport.Utils;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.TokenRing;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.TokenRingImpl;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.consumers.Consumer;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packet.NetworkPacket;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packet.NetworkPacketImpl;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packetholders.PacketHolder;
import org.openjdk.jmh.annotations.*;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * @author Vasyukevich Andrey
 * @since 26.11.2017
 */
public class Test_Size50_LinkedList {
    @State(Scope.Thread)
    public static class TokenRingState {
        public int size;
        public List<PacketHolder> packetHolders;
        public long timeout;
        public TimeUnit timeUnit;
        public TokenRing tokenRing;

        public int sourceNode;
        public int destinationNode;

        public CountDownLatch countDownLatch;
        public NetworkPacket packet;
        public Consumer packetConsumeNotifier;

        @Setup
        public void doSetup() {
            size = 50;
            packetHolders = Utils.createListOfPacketHoldersOnQueueBlocking(size, LinkedList.class);
            timeout = 1;
            timeUnit = TimeUnit.MILLISECONDS;

            tokenRing = new TokenRingImpl(size, packetHolders, timeout, timeUnit);

            sourceNode = 0;
            destinationNode = size - 1;
        }

        @Setup(Level.Invocation)
        public void doSetupBeforeInvocation() {
            countDownLatch = new CountDownLatch(1);
            packet = new NetworkPacketImpl(destinationNode, UUID.randomUUID().toString());
            packetConsumeNotifier = new PacketConsumeNotifier(packet, countDownLatch);

            tokenRing.attachConsumerToNode(destinationNode, packetConsumeNotifier);
        }

        @TearDown(Level.Invocation)
        public void doTearDownAfterInvocation() {
            tokenRing.detachConsumerFromNode(destinationNode, packetConsumeNotifier);
        }

        @TearDown
        public void doTearDown() {
            tokenRing.stop();
        }

        public void sendPacket() {
            tokenRing.getNodeAsConsumer(sourceNode).consume(packet);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.Throughput, Mode.AverageTime, Mode.SampleTime})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Fork(value = 3)
    public void test(TokenRingState state) throws InterruptedException {
        state.sendPacket();
        state.countDownLatch.await();
    }
}
