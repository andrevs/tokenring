package com.sbt.bit.sem3.multiprocprog.hw2.testing.manypackets;

import com.sbt.bit.sem3.multiprocprog.hw2.testing.testsupport.PacketConsumeNotifier;
import com.sbt.bit.sem3.multiprocprog.hw2.testing.testsupport.Utils;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.TokenRing;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.TokenRingImpl;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.consumers.Consumer;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packet.NetworkPacket;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packet.NetworkPacketImpl;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packetholders.PacketHolder;
import org.openjdk.jmh.annotations.*;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author Vasyukevich Andrey
 * @since 26.11.2017
 */
public class Test_Size100_LBQ_10ppn {
    @State(Scope.Thread)
    public static class TokenRingState {
        public int size;
        public List<PacketHolder> packetHolders;
        public long timeout;
        public TimeUnit timeUnit;
        public TokenRing tokenRing;

        public int producedPacketsPerNode;
        public List<Thread> producerThreads;

        public int sourceNode;
        public int destinationNode;

        public CountDownLatch countDownLatch;
        public NetworkPacket packet;
        public Consumer packetConsumeNotifier;

        @Setup
        public void doSetup() {
            size = 100;
            packetHolders = Utils.createListOfPacketHoldersOnBlockingQueue(size, LinkedBlockingQueue.class);
            timeout = 1;
            timeUnit = TimeUnit.MILLISECONDS;

            tokenRing = new TokenRingImpl(size, packetHolders, timeout, timeUnit);

            producedPacketsPerNode = 10;
            producerThreads = Utils.createListOfPacketProducersThreads(tokenRing, producedPacketsPerNode);
            for (Thread producerThread : producerThreads) {
                producerThread.start();
            }

            sourceNode = 0;
            destinationNode = size - 1;
        }

        @Setup(Level.Invocation)
        public void doSetupBeforeInvocation() {
            countDownLatch = new CountDownLatch(1);
            packet = new NetworkPacketImpl(destinationNode, UUID.randomUUID().toString());
            packetConsumeNotifier = new PacketConsumeNotifier(packet, countDownLatch);

            tokenRing.attachConsumerToNode(destinationNode, packetConsumeNotifier);
        }

        @TearDown(Level.Invocation)
        public void doTearDownAfterInvocation() {
            tokenRing.detachConsumerFromNode(destinationNode, packetConsumeNotifier);
        }

        @TearDown
        public void doTearDown() throws InterruptedException {
            tokenRing.stop();

            for (Thread producerThread : producerThreads) {
                producerThread.interrupt();
            }
            for (Thread producerThread : producerThreads) {
                producerThread.join();
            }
        }

        public void sendPacket() {
            tokenRing.getNodeAsConsumer(sourceNode).consume(packet);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.Throughput, Mode.AverageTime, Mode.SampleTime})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Fork(value = 3)
    public void test(TokenRingState state) throws InterruptedException {
        state.sendPacket();
        state.countDownLatch.await();
    }
}
