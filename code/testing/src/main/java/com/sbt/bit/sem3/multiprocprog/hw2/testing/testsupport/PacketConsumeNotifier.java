package com.sbt.bit.sem3.multiprocprog.hw2.testing.testsupport;

import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.consumers.Consumer;
import com.sbt.bit.sem3.multiprocprog.hw2.tokenring.packet.NetworkPacket;

import java.util.concurrent.CountDownLatch;

/**
 * @author Vasyukevich Andrey
 * @since 26.11.2017
 */
public class PacketConsumeNotifier implements Consumer {
    private final NetworkPacket networkPacket;
    private final CountDownLatch countDownLatch;

    public PacketConsumeNotifier(NetworkPacket networkPacket, CountDownLatch countDownLatch) {
        this.networkPacket = networkPacket;
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void consume(NetworkPacket packet) {
        if (networkPacket.equals(packet)) {
            countDownLatch.countDown();
        }
    }
}
